module.exports = {
  
    collectCoverage: true,
    collectCoverageFrom: ['src/**/*.{js,jsx}'],
  
    "verbose": true,
    "testEnvironment": "node",
    "transform": {
      "^.+\\.(js|jsx)$": "babel-jest"
    },
    "globals": {
      "NODE_ENV": "test"
    },
    "moduleFileExtensions": [
      "js",
      "jsx"
    ],
    "moduleDirectories": [
      "node_modules",
      "<rootDir>/src/client" // only client directory for now
    ],
    "moduleNameMapper":{
        "Components": "<rootDir>/src/client/components/",
        "Containers": "<rootDir>/src/client/containers/",
        "Services": "<rootDir>/src/client/services/",
        "Actions": "<rootDir>/src/client/actions/",
        "Reducers": "<rootDir>/src/client/reducers/",
        '^.+\\.(scss|jpg)$': '<rootDir>/src/client/test/__mocks__/fileMock.jsx'
    },
    "setupTestFrameworkScriptFile": "<rootDir>/test-config/enzyme/enzyme-setup.js"
  }
