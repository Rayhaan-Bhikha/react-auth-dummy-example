const express = require('express');
const path = require('path');
const app = express();

const ServerRendererPath = path.join(__dirname, '..', 'dist', 'server.js');
const ServerRenderer = require(ServerRendererPath).default;

const PORT = process.env.PORT || 9000;

app.use("/dist", express.static(path.join(__dirname, "..", "dist")));

app.get("/*", ServerRenderer);

app.listen(PORT, () => {
    console.log(`Application running on http://localhost:${PORT}`);
});