import React, { Component } from 'react';
import { Menu } from 'Components';
import {connect} from 'react-redux';
import {setValue} from '../rootActions'

class Home extends Component {
	render() {
		return (
			<div>
                <Menu/>
				<h1>Home</h1>
				<button onClick={this.props.setValue}>set value</button>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		state
	}
}

const mapDispatchToProps = dispatch => {
	return {
		setValue: () => dispatch(setValue(1))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);