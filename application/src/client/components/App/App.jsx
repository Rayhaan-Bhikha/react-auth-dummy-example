import React, { Component } from 'react';
import './App.scss';
import {Home, Login, Secret} from 'Components';
import { Switch, Route } from 'react-router-dom';

export default class App extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div>
				<Switch>
					<Route exact path='/' component={ Home } />
					<Route path="/login" component={ Login } />
					<Route path="/secret" component={ Secret } />
				</Switch>
			</div>
		);
	}
}