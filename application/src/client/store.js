import { createStore, combineReducers, applyMiddleware } from 'redux'
import logger from 'redux-logger'
import rootReducer from './rootReducer'

// Grab the state from a global variable injected into the server-generated HTML
const preloadedState = window.__PRELOADED_STATE__;
// Allow the passed state to be garbage-collected
delete window.__PRELOADED_STATE__;
// Create Redux store with initial state
const store = createStore(rootReducer, preloadedState, applyMiddleware(logger))

export default store;