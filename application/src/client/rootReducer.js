const initialState = {
    result: 0,
    lastValue: [0]
}

const rootReducer = (state=initialState, action) => {
    switch(action.type) {
        case 'SET_VALUE':
            state = {
                ...state,
                result: action.payload,
                lastValue: [...state.lastValue, action.payload]
            }
            break;
    }
    return state;
}

export default rootReducer;