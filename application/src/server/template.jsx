export default ({ markup, preloadedState }) => {
	return `<!doctype html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/dist/bundle.css">
<title>Sky - auth</title>
</head>
<body>
	<div id="root">${markup}</div>
	<script>
          // WARNING: See the following for security issues around embedding JSON in HTML:
          // http://redux.js.org/recipes/ServerRendering.html#security-considerations
          window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}
     </script>
	<script src="/dist/bundle.js"></script>
</body>
</html>`;
};