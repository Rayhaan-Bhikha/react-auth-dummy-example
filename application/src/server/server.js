import express from 'express';
import React from 'react';
import { renderToString } from 'react-dom/server';
import {StaticRouter} from "react-router-dom";
import {App} from 'Components';
import path from 'path';

const port = 9000;
const server = express();

server.use(express.static('.'));

server.get('/*', (req, res) => {

  const context = {};
  const jsx = (
    <StaticRouter context={context} location={req.url} >
      <App />
    </StaticRouter>
  );


  const body = renderToString(jsx);
  const title = 'SSR';

  res.writeHead( 200, { "Content-Type": "text/html" } );
  res.end(
    htmlTemplate({
      body,
      title
    })
  );
});

function htmlTemplate({ body, title }){
  return `<!DOCTYPE html>
  <html>
  <head>
      <meta charset="utf-8">
      <title>${title}</title>
      <link rel="stylesheet" type="text/css" href="/dist/bundle.css">
  </head>
  <body>
      <div id="app">${ body }</div>
      <script type="text/javascript" src="/dist/bundle.js"></script>
  </body>
  </html>
`;
};

server.listen(port, () => {
  console.log(`server started on port: ${port}`)
});