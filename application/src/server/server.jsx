import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import Template from './template';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import rootReducer from '../client/rootReducer'
import { App } from 'Components';

const serverRendererDev = ({ clientStats, serverStats, foo}) => {
	return (req, res, next) => {
		res.locals.reactApp = buildTemplate(req.url, createStore(rootReducer));
		next();
	};
}

const serverRendererProd = (req, res) => {
	res.send(buildTemplate(req.url, createStore(rootReducer)));
}

/**
 * @function
 * @param String url - for route
 * @param Object store - redux store
 * 
 */
function buildTemplate(url, store, context={}) {
	const markup = ReactDOMServer.renderToString(
		<Provider store={store}>
			<StaticRouter location={ url } context={ context }>
				<App />
			</StaticRouter>
		</Provider>
	);
	const preloadedState = store.getState();
	console.log("STATE: ", preloadedState);
	return Template({
		markup, 
		preloadedState
	});
}

var exportedFunction = null;
if(process.env.NODE_ENV === 'production') {
	exportedFunction = serverRendererProd;
} else {
	exportedFunction = serverRendererDev
}

export default exportedFunction;