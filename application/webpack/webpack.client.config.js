const path = require('path');

const CLIENT_DIR = path.join(__dirname, '..', 'src', "client");
const DIST_OUTPUT = path.join(__dirname, '..', 'dist');

const clientDevConfig = {
    name: 'client',
    target: 'web',
    entry: {
        bundle: ['babel-polyfill', path.join(CLIENT_DIR, 'client.jsx')],
    },
    output: {
        path: DIST_OUTPUT,
        filename: '[name].js',
        publicPath: '/dist/',
    }
}


module.exports = clientDevConfig;