const clientConf = require('./webpack.client.config.js')
const serverConf = require('./webpack.server.config.js')
const baseConfig = require('./webpack.base.config.js');
const merge = require('webpack-merge');
const webpack = require('webpack');

const devConfig = {
    mode: 'development',
    plugins: [
        new webpack.EnvironmentPlugin({
            'NODE_ENV': 'development'
        })
    ]
}

const client = merge(baseConfig, devConfig, clientConf);
const server = merge(baseConfig, devConfig, serverConf);
const config = [client, server];

module.exports = config;
