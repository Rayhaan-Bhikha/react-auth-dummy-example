const path = require('path');

const SERVER_DIR = path.join(__dirname, '..', 'src', "server");
const DIST_OUTPUT = path.join(__dirname, '..', 'dist');


const serverDevConfig = {
    name: 'server',
    target: 'node',
    entry: `${SERVER_DIR}/server.jsx`,
    output: {
        path: DIST_OUTPUT,
        filename: 'server.js',
        library: 'serverLib',
        libraryTarget: 'commonjs2',
        publicPath: '/dist/',
    }
}

module.exports = serverDevConfig;