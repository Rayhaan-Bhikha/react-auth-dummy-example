const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const path = require('path');
const DIST_DIR_OUTPUT = path.join(__dirname, '..', 'dist');
const SRC_DIR = path.resolve("src");
const CLIENT_DIR = path.join(SRC_DIR, "client");

const baseConfig = {
    mode: 'development',
    devtool: 'source-map',
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: "babel-loader",
                options: {
                    presets: ["env", "react", "stage-2", "stage-0"],
                    plugins: [require("babel-plugin-transform-class-properties")]
                }
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader, // creates style nodes from JS strings
                    "css-loader", // translates CSS into CommonJS
                    "sass-loader?includePaths[]=./node_modules" // compiles Sass to CSS
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    {
                        loader: 'file-loader'
                    }
                ]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin([DIST_DIR_OUTPUT], 
            {
                allowExternal: true
            }
        ),
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: "[name].css",
            chunkFilename: "[id].css"
        })
    ],
    resolve: {
        extensions: ['.js', '.jsx'],
        alias: {
            Components: path.join(CLIENT_DIR, "components")
        }
    }
}

module.exports = baseConfig;