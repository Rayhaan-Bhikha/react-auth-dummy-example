const clientConf = require('./webpack.client.config.js')
const serverConf = require('./webpack.server.config.js')
const baseConfig = require('./webpack.base.config.js');
const merge = require('webpack-merge');
const webpack = require('webpack')

const prodConfig = {
    mode: "production",
    plugins: [
        new webpack.EnvironmentPlugin({
            'NODE_ENV': 'production'
        })
    ]
}

const client = merge(baseConfig, prodConfig, clientConf);
const server = merge(baseConfig, prodConfig, serverConf);
module.exports = [client, server];